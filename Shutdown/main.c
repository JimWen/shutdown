#include <Windows.h>													//基本Windows头文件
#include <windowsx.h>													//对话框控件头文件
#include "resource.h"													//对话框资源头文件

#include "shutdown.h"													//关机操作函数头文件
#include "trayicon.h"													//对话框托盘化头文件
#include "autorun.h"													//程序开机自启动头文件
#include "config.h"														//参数写入读出头文件	
#include <commctrl.h>
#include <tchar.h>

/************************************************************************/
/* 全局变量和常量定义                                                   */
/************************************************************************/
#define ID_TIMER_CT 1	
#define ONE_INSTANCE_GUID TEXT("0xafce1ce3, 0xae0c, 0x4601, 0x9b, 0x44, 0x6e, 0xc8, 0x25, 0xa6, 0x87, 0x39") 


static TCHAR g_szExePath[MAX_PATH] ;						//程序本身路径
static TCHAR g_szConfigPath[MAX_PATH] ;						//定时配置文件路径
static TCHAR g_szSoftConfigPath[MAX_PATH] ;					//软件配置文件路径
static HINSTANCE g_hInstance;

static WORD g_hotkeyId;
static HANDLE g_hOneInstance;

/************************************************************************/
/* 函数原型声明                                                         */
/************************************************************************/
BOOL CALLBACK DlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam );
BOOL CALLBACK SetDlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam );
BOOL CALLBACK AboutDlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam );
void SetCurrentTime(HWND hDlg, int iID, SYSTEMTIME st);

/************************************************************************/
/* 解决程序对话框界面风格问题                                           */
/************************************************************************/
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

/************************************************************************/
/* Windows程序入口函数                                                  */
/************************************************************************/
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{

	WIN32_FIND_DATA fd;

	//获得当前可执行文件和配置文件路径
	TCHAR szDrive[10] ;								//可执行文件盘符
	TCHAR szDir[256] ;								//可执行文件目录
	TCHAR szFilename[64] ;							//可执行文件文件名
	TCHAR szExt[10] ;								//可执行文件文件后缀

	//应用程序单实例化
	g_hOneInstance = CreateMutex(NULL, FALSE, ONE_INSTANCE_GUID);//handle为声明的HANDLE类型的全局变量  
	if(GetLastError()==ERROR_ALREADY_EXISTS)  
	{
		MessageBox(NULL, TEXT("应用程序已经在运行"), TEXT("警告"), MB_OK);  
		return 0;  
	}

	GetModuleFileName(NULL, g_szExePath, MAX_PATH) ;
	_tsplitpath(g_szExePath, szDrive, szDir, szFilename, szExt) ;			//分割全路径
	wsprintf(g_szConfigPath, TEXT("%s%sshutdown.dat"), szDrive, szDir) ;	//得到定时配置文件路径	
	wsprintf(g_szSoftConfigPath, TEXT("%s%sconfig.dat"), szDrive, szDir) ;	//得到软件配置文件路径	

	//保证配置文件存在和路径正确
	if (INVALID_HANDLE_VALUE  == FindFirstFile(g_szConfigPath, &fd))
	{
		MessageBox(NULL, TEXT("程序定时配置文件丢失!"), TEXT("警告"), MB_OK | MB_ICONHAND);
	}
	if (INVALID_HANDLE_VALUE  == FindFirstFile(g_szSoftConfigPath, &fd))
	{
		MessageBox(NULL, TEXT("程序软件配置文件丢失!"), TEXT("警告"), MB_OK | MB_ICONHAND);
	}


	//弹出对话框
	DialogBox(hInstance, MAKEINTRESOURCE(IDD_MAIN), NULL, DlgProc ) ;

	g_hInstance = hInstance;
	return 0 ;
}

/************************************************************************/
/* 对话框过程函数                                                       */
/************************************************************************/
BOOL CALLBACK DlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	int nCxScreen;
	int nCyScreen;
	RECT rectWindow;

	int nIndex;
	SYSTEMTIME stCur;
	SYSTEMTIME stDate, stTime, stSet;
	FILETIME ft;
	ULARGE_INTEGER lnTime;
	TIMEMODEL tmModel;
	TIMEOPERATE tmOperate;

	POINT pt ;
	HMENU hMenu ;

	static BOOL bIsStart=FALSE;//软件启动时标志，此时在WM_NCPAINT中隐藏窗口

	switch (message)
	{
	case WM_INITDIALOG:
		SendMessage(hwnd, 
			        WM_SETICON, 
					ICON_SMALL2,
					(LPARAM)LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_APP)));
		//SetClassLong(hwnd, GCL_HICON, (long)LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_APP)));//有效  

		//窗体默认显示在屏幕中间
		GetWindowRect(hwnd, &rectWindow);
		nCxScreen = GetSystemMetrics(SM_CXSCREEN);
		nCyScreen = GetSystemMetrics(SM_CYSCREEN);

		MoveWindow(hwnd,
				  (nCxScreen-(rectWindow.right-rectWindow.left))/2,
				  (nCyScreen-(rectWindow.bottom-rectWindow.top))/2,
				  rectWindow.right-rectWindow.left,
				  rectWindow.bottom-rectWindow.top,
				  FALSE);

		//设置定时模式范围
		ComboBox_AddString(GetDlgItem(hwnd, IDC_TIMEMODEL), TEXT("仅此一次")) ;
		ComboBox_AddString(GetDlgItem(hwnd, IDC_TIMEMODEL), TEXT("每天一次")) ;
		ComboBox_AddString(GetDlgItem(hwnd, IDC_TIMEMODEL), TEXT("每周一次")) ;
		ComboBox_SetCurSel(GetDlgItem(hwnd, IDC_TIMEMODEL), 0);

		//设置定时操作范围
		ComboBox_AddString(GetDlgItem(hwnd, IDC_TIMEOPERATE), TEXT("关机")) ;
		ComboBox_AddString(GetDlgItem(hwnd, IDC_TIMEOPERATE), TEXT("重启")) ;
		ComboBox_AddString(GetDlgItem(hwnd, IDC_TIMEOPERATE), TEXT("注销")) ;
		ComboBox_SetCurSel(GetDlgItem(hwnd, IDC_TIMEOPERATE), 0);

		//读入配置文件
		ReadConfig(g_szConfigPath);
		ReadSoftwareConfig(g_szSoftConfigPath);

		//显示相应的定时列表
		ShowToListbox(GetDlgItem(hwnd, IDL_TIMELIST));

		//托盘化
		if (FALSE == g_bIsHide)
		{
			ToTray(hwnd, IDI_APP);
		}

		//开机自动启动
		if (g_bIsAutoRun)
		{
			SetAutoRun(TEXT("Jimwen-Shutdown"), g_szExePath);
		}

		//注册热键
		g_hotkeyId = GlobalAddAtom(TEXT("Jimwen-Shutdown"));
		if (FALSE == RegisterHotKey(hwnd, g_hotkeyId, g_nModifier, g_nKey))
		{
			MessageBox(hwnd, TEXT("热键已被占用，请再设置其他热键!"), TEXT("警告"), MB_OK | MB_ICONHAND);
		}

		//开启定时
		SetTimer(hwnd, ID_TIMER_CT, 1000, NULL);
		
		return TRUE ;

	case WM_NCPAINT:
		//默认启动时不显示主窗口
		if (FALSE == bIsStart)
		{
			ShowWindow(hwnd, SW_HIDE);
			bIsStart = TRUE;
		}

		return FALSE;


	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		//点击关闭对话框隐藏到托盘
		case IDCANCEL:													
			ShowWindow(hwnd, SW_HIDE);
			return TRUE ;

		//点击退出按钮退出程序
		case IDB_EXIT:
			KillTimer(hwnd, ID_TIMER_CT) ;			//关闭定时器
			DeleteTray(hwnd);
			GlobalDeleteAtom(g_hotkeyId);
			EndDialog(hwnd, 0) ;					//关闭对话框
			return TRUE;

		case IDB_ADD:
			//获得数据
			DateTime_GetSystemtime(GetDlgItem(hwnd, IDC_DATE), &stDate);
			DateTime_GetSystemtime(GetDlgItem(hwnd, IDC_TIME), &stTime);
			stSet.wYear = stDate.wYear;
			stSet.wMonth = stDate.wMonth;
			stSet.wDay = stDate.wDay;
			stSet.wHour = stTime.wHour;
			stSet.wMinute = stTime.wMinute;
			stSet.wSecond = stTime.wSecond;
			stSet.wMilliseconds = stTime.wMilliseconds;

			SystemTimeToFileTime(&stSet, &ft);
			lnTime.HighPart = ft.dwHighDateTime;
			lnTime.LowPart= ft.dwLowDateTime;

			tmModel = (TIMEMODEL)ComboBox_GetCurSel(GetDlgItem(hwnd, IDC_TIMEMODEL));
			tmOperate = (TIMEOPERATE)ComboBox_GetCurSel(GetDlgItem(hwnd, IDC_TIMEOPERATE));

			//添加到定时列表
			AddTimeNode(lnTime, tmModel, tmOperate);

			//刷新显示
			ShowToListbox(GetDlgItem(hwnd, IDL_TIMELIST));

			//保存到配置文件
			SetConfig(g_szConfigPath);
			return TRUE;

		case IDB_DELETE:
			//删除指定项
			RemoveTimeNode(ListBox_GetCurSel(GetDlgItem(hwnd, IDL_TIMELIST)));

			//刷新显示
			ShowToListbox(GetDlgItem(hwnd, IDL_TIMELIST));

			//保存到配置文件
			SetConfig(g_szConfigPath);
			return TRUE;

		case IDB_ABOUT:
			//弹出关于对话框
			DialogBox(g_hInstance, MAKEINTRESOURCE(IDD_ABOUT), hwnd, AboutDlgProc ) ;
			return TRUE;

		case IDB_SET:
			//弹出设置对话框
			DialogBox(g_hInstance, MAKEINTRESOURCE(IDD_SETTING), hwnd, SetDlgProc ) ;
			return TRUE ;
		}
		break ;

	//托盘消息处理
	case WM_TRAYMESSAGE:
		switch (lParam)
		{
		//双击左键的处理
		case WM_LBUTTONDBLCLK:
			SetForegroundWindow(hwnd);
			ShowWindow(hwnd, SW_NORMAL) ;//显示主窗口
			return TRUE ;

		//右键弹出菜单
		case WM_RBUTTONUP:
			GetCursorPos(&pt) ;
			hMenu = CreatePopupMenu() ;									
			AppendMenu(hMenu, MF_STRING, IDB_SET, TEXT("设置...")) ;
			AppendMenu(hMenu, MF_STRING, IDB_ABOUT, TEXT("关于...")) ;
			AppendMenu(hMenu, MF_SEPARATOR,0, 0);
			AppendMenu(hMenu, MF_STRING, IDB_EXIT, TEXT("退出")) ;
			TrackPopupMenu(hMenu, TPM_LEFTBUTTON, pt.x, pt.y, 0, hwnd, NULL) ;
			return TRUE ;
		}

		return FALSE;
		
	case WM_HOTKEY:
		SetForegroundWindow(hwnd);
		ShowWindow(hwnd, SW_NORMAL) ;//显示主窗口
		return TRUE;
		
	case WM_TIMER:
		GetLocalTime(&stCur);

		//更新当前时间
		SetCurrentTime(hwnd, IDE_CURTIME, stCur);

		//判断是否到时间
		if (-1 != (nIndex = IsOnTime(stCur)))
		{
			OperateOnTime(nIndex, GetDlgItem(hwnd, IDL_TIMELIST), g_szConfigPath);
		}
		return TRUE ;
	}

	return FALSE ;
}

/************************************************************************/
/* 
** 功能:获得并在指定位置显示当前时间
** 输入:hDlg	对话框句柄
**		iID		要显示时间的子空间ID号
** 输出:SYSTEMTIME结构表示的当前时间
*/
/************************************************************************/
void SetCurrentTime(HWND hDlg, int iID, SYSTEMTIME st)
{
	TCHAR szBuffer[30] ;
	TCHAR szDayOfWeek[30] ;

	switch (st.wDayOfWeek)
	{
	case 1:
		wsprintf(szDayOfWeek, TEXT("周一")) ;
		break;
	case 2:
		wsprintf(szDayOfWeek, TEXT("周二")) ;
		break;
	case 3:
		wsprintf(szDayOfWeek, TEXT("周三")) ;
		break;
	case 4:
		wsprintf(szDayOfWeek, TEXT("周四")) ;
		break;
	case 5:
		wsprintf(szDayOfWeek, TEXT("周五")) ;
		break;
	case 6:
		wsprintf(szDayOfWeek, TEXT("周六")) ;
		break;
	case 0:
		wsprintf(szDayOfWeek, TEXT("周日")) ;//注意周末对应索引号为0
		break;
	}

	//显示时间
	wsprintf(szBuffer, 
		     TEXT("%04d年%02d月%02d日 %02d:%02d:%02d	%s"),
			 st.wYear,
			 st.wMonth,
			 st.wDay,
			 st.wHour,
			 st.wMinute,
			 st.wSecond,
			 szDayOfWeek);
	SetDlgItemText(hDlg, iID, szBuffer) ;								
}

BOOL CALLBACK SetDlgProc( HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	UINT modifier=0;
	WORD wHotkey; 

	switch(message)
	{
	case WM_INITDIALOG:
		{
			//转换Key Modifier
			modifier=0;
			if(g_nModifier & MOD_ALT)
			{
				modifier |= HOTKEYF_ALT;
			}
			if(g_nModifier & MOD_CONTROL)
			{
				modifier |= HOTKEYF_CONTROL;
			}
			if(g_nModifier & MOD_SHIFT)
			{
				modifier |= HOTKEYF_SHIFT;
			}

			//设置初始化界面
			CheckDlgButton(hwnd, IDC_HIDE, g_bIsHide);
			CheckDlgButton(hwnd, IDC_AUTORUN, g_bIsAutoRun);
			
			SendMessage(GetDlgItem(hwnd, IDH_HOTKEY),
						HKM_SETHOTKEY ,
						MAKEWPARAM(MAKEWORD((BYTE)g_nKey, (BYTE)modifier) ,0),
						0);
		}
		return (TRUE);

	case WM_CLOSE:
		{
			EndDialog(hwnd,0);
		}
		return (TRUE);

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDCANCEL:
			{
				SendMessage(hwnd, WM_CLOSE, 0, 0);
			}
			return (TRUE);
		case IDOK:
			{	
				//获得界面参数
				g_bIsHide = BST_CHECKED == IsDlgButtonChecked(hwnd, IDC_HIDE) ? TRUE : FALSE;
				g_bIsAutoRun = BST_CHECKED == IsDlgButtonChecked(hwnd, IDC_AUTORUN) ? TRUE : FALSE;
				wHotkey = (WORD) SendMessage(GetDlgItem(hwnd, IDH_HOTKEY), HKM_GETHOTKEY, 0, 0);

				//分拆出热键
				g_nKey = LOBYTE(wHotkey);
				modifier = HIBYTE(wHotkey);
				g_nModifier=0;
				if(modifier & HOTKEYF_ALT)
				{
					g_nModifier |= MOD_ALT;
				}
				if(modifier & HOTKEYF_CONTROL)
				{
					g_nModifier |= MOD_CONTROL;
				}
				if(modifier & HOTKEYF_SHIFT)
				{
					g_nModifier |= MOD_SHIFT;
				}

				//托盘化
				if (FALSE == g_bIsHide)
				{
					ToTray(GetParent(hwnd), IDI_APP);
				}
				else
				{
					DeleteTray(GetParent(hwnd));
					ShowWindow(GetParent(hwnd), SW_HIDE);
				}

				//开机自动启动
				if (g_bIsAutoRun)
				{
					SetAutoRun(TEXT("Jimwen-Shutdown"), g_szExePath);
				}
				else
				{
					CancelAutoRun(TEXT("Jimwen-Shutdown"));
				}

				//注册热键
				UnregisterHotKey(GetParent(hwnd), g_hotkeyId);//先取消前一次
				if (FALSE == RegisterHotKey(GetParent(hwnd), g_hotkeyId, g_nModifier, g_nKey))
				{
					MessageBox(hwnd, TEXT("热键已被占用，请再设置其他热键!"), TEXT("警告"), MB_OK | MB_ICONHAND);
				}
				else
				{
					SetSoftwareConfig(g_szSoftConfigPath);
					SendMessage(hwnd, WM_CLOSE, 0, 0);
				}
			}
			return (TRUE);
		}
		return (FALSE);
	}

	return (FALSE);
}

BOOL CALLBACK AboutDlgProc( HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch(message)
	{
	case WM_INITDIALOG:
        {
			
        }
		return (TRUE);

	case WM_CLOSE:
        {
            EndDialog(hwnd, 0);
        }
		return (TRUE);
	case WM_NOTIFY:

		switch (((LPNMHDR)lParam)->code)
		{

		case NM_CLICK:          // Fall through to the next case.

		case NM_RETURN:
			{
				PNMLINK pNMLink = (PNMLINK)lParam;
				LITEM   item    = pNMLink->item;

				if ((((LPNMHDR)lParam)->hwndFrom == GetDlgItem(hwnd, IDB_SYSLINK)))
				{
					ShellExecute(NULL, TEXT("open"), item.szUrl, NULL, NULL, SW_SHOWNORMAL);
				}

				break;
			}
		}

		return TRUE;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
            {
                SendMessage(hwnd, WM_CLOSE, 0, 0);
            }
			return (TRUE);
		}
		return (FALSE);
	}
	return (FALSE);
}
