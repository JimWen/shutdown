#include <Windows.h>
#include "autorun.h"

#pragma comment(lib, "Advapi32")//注册表操作函数库

/**
 *功能:将指定路径的可执行文件加到开机启动中
 *参数:szRegName--注册表名
	   szExePath--可执行文件完整路径
 *返回:成功返回TRUE,失败返回FALSE
 *其他:2014/01/12 By Jim Wen Ver1.0
**/
BOOL SetAutoRun(LPCTSTR szRegName, LPCTSTR szExePath)
{
	TCHAR	szRegPos[MAX_PATH] = TEXT("Software\\Microsoft\\Windows\\CurrentVersion\\Run");		
	HKEY	hRegKey;
	BOOL	bResult;

	if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, szRegPos, 0, KEY_ALL_ACCESS, &hRegKey) != ERROR_SUCCESS)
	{
		bResult = FALSE;
	}
	else
	{
		if(RegSetValueEx(hRegKey, szRegName, 0, REG_SZ, (BYTE*)szExePath, (lstrlen(szExePath)+1) * sizeof(TCHAR)) != ERROR_SUCCESS)
		{
			bResult = FALSE;
		}
		else
		{
			bResult = TRUE;
			RegCloseKey(hRegKey);
		}
	}

	return bResult;
}

/**
 *功能:取消指定注册表名称的可执行文件的开机启动
 *参数:szRegName--注册表名称
 *返回:成功返回TRUE,失败返回FALSE
 *其他:2014/01/12 By Jim Wen Ver1.0
**/
BOOL CancelAutoRun(LPCTSTR szRegName)
{
	TCHAR	szRegPos[MAX_PATH] = TEXT("Software\\Microsoft\\Windows\\CurrentVersion\\Run");	
	HKEY	hRegKey;
	BOOL	bResult;

	if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, szRegPos, 0, KEY_ALL_ACCESS, &hRegKey) != ERROR_SUCCESS)
	{
		bResult = FALSE;
	}
	else
	{
		RegDeleteValue(hRegKey, szRegName) ;							//删除注册表键值
		RegDeleteKey(hRegKey, szRegName) ;								//删除注册表键
		if(RegCloseKey(hRegKey) == ERROR_SUCCESS)						//删除注册表句柄
		{
			bResult = TRUE ;
		}
		else
		{
			bResult =  FALSE ;
		}	
	}

	return bResult;
}