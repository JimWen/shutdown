#ifndef CONFIG_H_H_H
#define CONFIG_H_H_H

/************************************************************************/
/* 常量和全局变量                                                       */
/************************************************************************/
#define MAX_COUNT 255
typedef enum tagTIMEMODEL
{
	TIMEMODEL_ONCE=0,
	TIMEMODEL_EVERYDAY,
	TIMEMODEL_EVERYWEEK
}TIMEMODEL;
typedef enum tagTIMEOPERATE
{
	TIMEOPERATE_SHUTDOWN=0,
	TIMEOPERATE_RESTART,
	TIMEOPERATE_LOGOFF
}TIMEOPERATE;
typedef struct tagTIMENODE
{
	ULARGE_INTEGER time;
	TIMEMODEL model;
	TIMEOPERATE operate;
}TIMENODE;

//软件行为配置文件
extern BOOL g_bIsHide;								//是否隐藏到后台
extern BOOL g_bIsAutoRun;							//是否自动启动
extern UINT g_nModifier;							//热键转换
extern UINT g_nKey;									//热键键值

/************************************************************************/
/* 定义函数原型                                                         */
/************************************************************************/
BOOL SetConfig(LPCTSTR szConfigFile);
BOOL ReadConfig(LPCTSTR szConfigFile);
BOOL SetSoftwareConfig(LPCTSTR szConfigFile);
BOOL ReadSoftwareConfig(LPCTSTR szConfigFile);
BOOL AddTimeNode(const ULARGE_INTEGER time, const TIMEMODEL timeModel, const TIMEOPERATE timeOperate);
BOOL RemoveTimeNode(const UINT nIndex);
void ShowToListbox(const HWND hCtrl);
int IsOnTime(const SYSTEMTIME st);
void OperateOnTime(const int nIndex, const HWND hCtrl, LPCTSTR szConfigFile);

#endif